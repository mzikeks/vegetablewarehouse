﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VegetableWarehouse
{
    class ParseAddingException : Exception 
    {
        public ParseAddingException() : base("Ошибка при разборе аргументов команды add! Проверьте ввод. Вы всегда можете воспользоваться командой help")
        { }
    }
    class Storage
    {
        private List<Container> containers = new List<Container>();
        private int firstContainerIndex = 0;
        public int nextId = 1;
        private int rent;
        private int capacity;

        public Storage(int capacity, int rent)
        {
            this.capacity = capacity;
            this.rent = rent;
        }

        public void Add(Container container)
        {
            var random = new Random();
            double damage = random.NextDouble() / 2;
            Console.WriteLine($"Цена контейнера без учета повреждений {container.Price}");
            container.Price *= 1 - damage;
            if (container.Price < rent)
            {
                throw new Exception("Контейнер не размещен на складе! Его цена меньше чем плата за рамещение");
            }

            if (containers.Count >= capacity)
            {
                Console.WriteLine($"Склад запонлнен! Удаляю {containers[firstContainerIndex]}");
                firstContainerIndex++;
            }

            containers.Add(container);
            Console.WriteLine($"Добавлен {containers[containers.Count-1]}!");
        }

        internal void PrintInfo(int id, string filename = null)
        {
            var output = new StringBuilder();
            if (containers.Count == 0)
            {
                throw new Exception("На складе пока нет контейнеров");
            }

            else if (id == -1)
            {
                for (int i = firstContainerIndex; i < containers.Count; i++)
                {
                    if (containers[i].InStorage)
                    {
                        Console.WriteLine(containers[i]);
                        output.Append(containers[i] + Environment.NewLine);
                    }
                }
            }
            else if (id == 0)
            {
                Console.WriteLine(containers[containers.Count - 1]);
                output.Append(containers[containers.Count - 1]);
            }

            else if (id > firstContainerIndex && id <= containers.Count)
            {
                if (containers[id - 1].InStorage)
                {
                    Console.WriteLine(containers[id - 1]);
                    output.Append(containers[id - 1]);
                }
                else
                {
                    throw new Exception("Ошибка! Контейнера с таким id нет на складе");
                }
            }
            else
            {
                throw new Exception("Ошибка в вводе аргументов для команды print! Воспользуйтесь командой help для помощи");
            }

            if (filename != null)
            {
                try
                {
                    File.AppendAllText(filename, output.ToString());
                    Console.WriteLine("Результат успешно записан в файл");
                }
                catch
                {
                    throw new Exception("Ошибка! Невозможно получить достут к указанному файлу");
                }
            }
        }

        internal void Delete(int id)
        {
            if (id == -1 || id == containers.Count)
            {
                if (containers.Count - 1 >= 0)
                {
                    Console.WriteLine($"{containers[containers.Count - 1]} успешно удалён!");
                    containers.RemoveAt(containers.Count - 1);
                }
                else
                {
                    throw new Exception("На складе пока нет контейнеров");
                }
            }
            else if (id > firstContainerIndex && id < containers.Count)
            {
                if (!containers[id-1].InStorage)
                {
                    throw new Exception("Ошибка! На складе нет контейнера с таким id");
                }
                else
                {
                    containers[id-1].RemoveFromStorage();
                    Console.WriteLine($"{containers[id-1]} успешно удалён");
                }
            }
            else
            {
                throw new Exception("Ошибка! Не существует контейнера с таким id");
            }
        }

        internal void PrintState(string filename = null)
        {
            int countContainers = 0;
            double sumPrice = 0;
            int sumWeight = 0;

            for (int i = firstContainerIndex; i < containers.Count; i++)
            {
                if (containers[i].InStorage)
                {
                    countContainers++;
                    sumPrice += containers[i].Price;
                    sumWeight += containers[i].Weight;
                }
            }
            string output = Environment.NewLine + $"Текущее состояние склада:" + Environment.NewLine +
                              $"Всего контейнерова {countContainers}" + Environment.NewLine +
                              $"Суммарный вес {sumWeight}" + Environment.NewLine +
                              $"Суммарная стоимость {Math.Round(sumPrice, 3)}" + Environment.NewLine;
            Console.WriteLine(output);

            if (filename != null)
            {
                try
                {
                    File.AppendAllText(filename, output.ToString() + Environment.NewLine);
                    Console.WriteLine("Результат успешно записан в файл");
                }
                catch
                {
                    throw new Exception("Ошибка! Невозможно получить достут к указанному файлу");
                }
            }
        }
    }
}
