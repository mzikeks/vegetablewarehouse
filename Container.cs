﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VegetableWarehouse
{
    class Container
    {
        private double price;
        private bool inStorage = true;
        private double rentability;
        private int id;
        private int weight;

        public Container(List<Box> boxes, int maxWeight, int id)
        {
            this.id = id;
            foreach (Box box in boxes)
            {
                if (Weight + box.Weight <= maxWeight)
                {
                    Weight += box.Weight;
                    Price += box.Price * box.Weight;
                }
                else
                {
                    Console.WriteLine($"Ящик с весом {box.Weight} не добавлен в контейнер (недостаточно места)");
                }
            }
        }

        public void RemoveFromStorage()
        {
            InStorage = false;
        }

        public double Price
        {
            get { return Math.Round(price, 2); }
            set { price = value; }
        }

        public int Weight
        {
            get { return weight; }
            private set { weight = value; }
        }
        public bool InStorage
        {
            get { return inStorage; }
            private set { inStorage = value; }
        }

        public override string ToString()
        {
            return $"Контейнер {id}: общий вес {Weight}, общая стоимость с учетом повреждений {Price}";
        }
    }
}
