﻿using System;
using System.Collections.Generic;
using System.IO;

namespace VegetableWarehouse
{
    class ExitException: Exception { }
    class Program
    {
        public static Storage storage;

        static void Main()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Для считывания данных из файла введите 1." + Environment.NewLine +
                                "Для считывания данных из консоли введите любую другую строку" + Environment.NewLine +
                                "Для выхода из программы введите 0");
                string input = Console.ReadLine();
                if (input == "0")
                {
                    break;
                }
                else if (input == "1")
                {
                    StartFileProgramm();
                }
                else
                {
                    try
                    {

                        StartConsoleProgramm();
                    }
                    catch (ExitException)
                    {
                        continue;
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Ошибка! Что-то пошло не так" + e.Message);
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                }
            }
        }

        private static void MainCycleConsole()
        {
            while (true)
            {
                bool isCommandExist = false;
                Random random = new Random();
                int maxWeight = random.Next(50, 1000);
                Console.WriteLine($"Максимальный вес следующего контейнера {maxWeight}");
                Console.Write("Введите команду: ");
                string command = Console.ReadLine();
                Console.WriteLine();

                TryRunExitCommand(command, ref isCommandExist);
                TryRunHelpCommand(command, ref isCommandExist);
                TryRunPrintStorageInfoCommand(command, ref isCommandExist);
                try
                {
                    TryRunAddCommand(command, ref isCommandExist, maxWeight);
                    TryRunDeleteCommand(command, ref isCommandExist);
                    TryRunPrintInfoCommand(command, ref isCommandExist);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message + Environment.NewLine);
                    Console.ResetColor();
                }
                if (!isCommandExist)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка! Такой комнады не существует!" + Environment.NewLine);
                    Console.ResetColor();
                }
                Console.WriteLine();
            }
        }

        private static void MainCycleFile()
        {
            string[] commands;
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите имя файла, где записаны команды");
                    commands = File.ReadAllLines(Console.ReadLine());
                    break;
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка при чтении данных из указанного файла!" + Environment.NewLine);
                    Console.ResetColor();
                }
            }

            foreach (string command in commands)
            {
                bool isCommandExist = false;
                Random random = new Random();
                int maxWeight = random.Next(50, 1000);
                Console.WriteLine(Environment.NewLine + $"Ваша команда: {command}");
                TryRunExitCommand(command, ref isCommandExist);
                TryRunHelpCommand(command, ref isCommandExist);
                TryRunPrintStorageInfoCommand(command, ref isCommandExist);
                try
                {
                    TryRunAddCommand(command, ref isCommandExist, maxWeight);
                    TryRunDeleteCommand(command, ref isCommandExist);
                    TryRunPrintInfoCommand(command, ref isCommandExist);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message + Environment.NewLine);
                    Console.ResetColor();
                }
                if (!isCommandExist)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка! Такой комнады не существует!" + Environment.NewLine);
                    Console.ResetColor();
                }
            }
            Console.WriteLine("Конец считываемого файла" + Environment.NewLine);
            Console.WriteLine("Для продолжения нажмите Enter");
            Console.ReadLine();
        }

        private static void StartConsoleProgramm()
        {
            Console.Clear();
            int capacity;
            Console.Write("Введите натуральное число - вместимость склада: ");
            while (!int.TryParse(Console.ReadLine(), out capacity) || capacity < 1)
            {
                Console.WriteLine("Ошибка! Введено некорректное число");
                Console.Write("Введите натуральное число - вместимость склада: ");
            }

            int rent;
            Console.Write("Введите натуральное число - стоимость хранения одного контейнера на складе: ");
            while (!int.TryParse(Console.ReadLine(), out rent) || rent < 1)
            {
                Console.WriteLine("Ошибка! Введено некорректное число");
                Console.Write("Введите натуральное число - стоимость хранения одного контейнера на складе: ");
            }
            PrintHelp();

            storage = new Storage(capacity, rent);
            MainCycleConsole();
        }

        private static void StartFileProgramm()
        {
            Console.Clear();
            int capacity;
            int rent;

            while (true)
            {
                Console.WriteLine("Введите путь к файлу, где записаны характеристики склада. Формат данных:" + Environment.NewLine +
                "В первой строке одно натуральное число - вместимость склада" + Environment.NewLine +
                "Во второй строке одно натуральное число - стоимость хранения одного контейнера на складе");
                try
                {
                    string[] inputStorage = File.ReadAllLines(Console.ReadLine());
                    capacity = int.Parse(inputStorage[0]);
                    rent = int.Parse(inputStorage[1]);
                    break;
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка ввода! Неправильный формат входного файла или к нему невозможно получить доступ" + Environment.NewLine);
                    Console.ResetColor();
                }
            }

            Console.WriteLine("Данные считаны!");
            storage = new Storage(capacity, rent);

            PrintHelp();

            Console.WriteLine("Составьте список команд и запишите его в файл, по одной команде в строке" + Environment.NewLine +
                              "Id контейнера - это то, каким по порядку добавили контейнер командой add");
            MainCycleFile();
        }

        private static void TryRunPrintStorageInfoCommand(string command, ref bool isCommandExist)
        {
            string[] input = command.Split();
            if (input[0] == "info")
            {
                isCommandExist = true;
                if (input.Length == 1)
                {
                    storage.PrintState();
                }
                else if (input.Length == 2)
                {
                    storage.PrintState(input[1]);
                }
                else
                {
                    throw new Exception("Ошибка при вводе команды info. У команды info может быть максимум 1 аргумент - имя файла, куда нужно записать состояние склада");
                }
                
            }
        }

        private static void TryRunHelpCommand(string command, ref bool isCommandExist)
        {
            if (command == "help")
            {
                isCommandExist = true;
                PrintHelp();
            }
        }

        private static void TryRunExitCommand(string command, ref bool isCommandExist)
        {
            if (command == "exit")
            {
                isCommandExist = true;
                Console.Clear();
                throw new ExitException();
            }
        }

        private static void TryRunPrintInfoCommand(string command, ref bool isCommandExist)
        {
            string[] input = command.Split();
            if (input[0] == "print")
            {
                isCommandExist = true;
                if (input.Length == 1)
                {
                    storage.PrintInfo(0);
                }
                else if (input.Length == 2 && int.TryParse(input[1], out int id) && (id > 0 || id == -1))
                {
                    storage.PrintInfo(id);
                }
                else if (input.Length == 2)
                {
                    storage.PrintInfo(0, input[1]);
                }
                else if (input.Length == 3 && int.TryParse(input[1], out int id1) && (id1 > 0 || id1 == -1))
                {
                    storage.PrintInfo(id1, input[2]);
                }
                else
                {
                    throw new Exception("Ошибка! Команда print должна содержать от 0 до 2 аргументов: id (натуральное число) и/или путь к файлу, куда нужно сохранить результат"); 
                }
            }
        }

        private static void TryRunDeleteCommand(string command, ref bool isCommandExist)
        {
            string[] input = command.Split();
            if (input[0] == "del")
            {
                isCommandExist = true;
                if (input.Length == 1)
                {
                    storage.Delete(-1);
                }
                else if (input.Length == 2 && int.TryParse(input[1], out int id) && id > 0)
                {
                    storage.Delete(id);
                }
                else
                {
                    throw new Exception("Ошибка при вводе команды del. Воспользуйтесь командой help для помощи");
                }
            }
        }

        private static void TryRunAddCommand(string command, ref bool isCommandExist, int maxWeight)
        {
            string[] input = command.Split(":");
            string[] inputFirst = input[0].Split();

            if (input.Length != 2)
            {
                return;
            }
            else if (inputFirst[0] == "add")
            {
                isCommandExist = true;
                int n = int.Parse(inputFirst[1]);
                if (n != input[1].Split(";").Length)
                {
                    throw new ParseAddingException();
                }

                var boxes = new List<Box>();
                foreach (string boxInput in input[1].Split(";"))
                {
                    string[] boxInputArray = boxInput.Trim().Split();
                    if (boxInputArray.Length != 2)
                    {
                        throw new ParseAddingException();
                    }
                    try
                    {
                        int boxWeight = int.Parse(boxInputArray[0]);
                        int boxPrice = int.Parse(boxInputArray[1]);
                        boxes.Add(new Box(boxWeight, boxPrice));
                    }
                    catch
                    {
                        throw new ParseAddingException();
                    }
                }
                try
                {
                    storage.Add(new Container(boxes, maxWeight, storage.nextId++));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    storage.nextId--;
                }
            }
        }

        private static void PrintHelp()
        {
            Console.WriteLine();
            Console.WriteLine("Добро пожаловать на склад!" + Environment.NewLine +
                              "Доступные команды" + Environment.NewLine +
                              "1. del " + Environment.NewLine +
                              "\t- без аргументов - удалить последний добавленный контейнер" + Environment.NewLine +
                              "\t- один аргумент (число) удалить контейнер с указанным id" + Environment.NewLine + Environment.NewLine +
                              "2. add *n - число ящиков*: *вес ящика 1* *цена за килограмм*;*...* *...*;*вес ящика n* *цена за килограмм*" + Environment.NewLine +
                              "\t например add 3: 5 1;2 4;1 1000" + Environment.NewLine + Environment.NewLine +
                              "3. print" + Environment.NewLine +
                              "\t- без аргументов - информация о последнем добавленном контейнере" + Environment.NewLine +
                              "\t- один аргумент (натуральное число) информация о контейнере с указанным id" + Environment.NewLine +
                              "\t- -1 - информация о всех контейнерах на складе" + Environment.NewLine + Environment.NewLine +
                              "4. exit - закончить текущую итерацию, обнулить склад" + Environment.NewLine + Environment.NewLine +
                              "5. info - напечатать информацию о текущем состоянии склада" + Environment.NewLine +
                              "Команды print и info могут принимать необязательный аргумент - путь к файлу, куда нужно продублировать результат выполнения команды" + Environment.NewLine +
                              "В файл добавится результат только в том случае, если команда будет выполнена, файл указывается последним аргументом" + Environment.NewLine);
        }
       
    }
}