﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VegetableWarehouse
{
    class Box
    {
        private int weight;
        private int price;

        public Box(int boxWeight, int boxPrice)
        {
            Weight = boxWeight;
            Price = boxPrice;
        }


        public int Price
        {
            get { return price; }
            private set { price = value; }
        }

        public int Weight
        {
            get { return weight; }
            private set { weight = value; }
        }
    }
}
